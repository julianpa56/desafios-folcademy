import { Component, Input, OnInit } from '@angular/core';
import { CardsInfo } from 'src/app/interfaces/cardsInfo';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MoviePopular } from 'src/app/interfaces/MoviePopular';
import { TvPopular } from 'src/app/interfaces/TvPopular';
import { Trending } from 'src/app/interfaces/Trending';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [MoviesService]
})
export class InicioComponent implements OnInit {

  auxTrending: Trending | any
  auxMovies : MoviePopular | any
  auxSeries : TvPopular | any  

  auxResultados : any = []
  auxBuscador : any = []
  buscador : string = ''

  cantidad : number = 0
  filtro : string = 'Todos'

  constructor( private _movieService : MoviesService ) { }

  ngOnInit(): void {

  this.getTrending()

  }

  encontrarResultados(resultados:any){
    this.auxResultados= resultados
  }

  getTrending() {
    this._movieService.getTrending().subscribe(
      {
        next: (data:Trending) => {
          this.auxTrending=data
          this.auxResultados = this.auxTrending['results']
          this.auxBuscador = this.auxResultados
          this.filtro='Todos'
          
        }, // se ejecuta si sale bien la consulta
        error: (err) => {
          console.log(err)
        },
        complete: () => {
          console.log('Ejecucion completa')
        }, //opcional
      }
    )
    this.cantidad = this.auxBuscador.length
  }

  getMovies() {
    this._movieService.getMovies().subscribe(
      {
        next: (data:MoviePopular) => {
          this.auxMovies=data
          this.auxResultados = this.auxMovies['results']
          this.auxBuscador = this.auxResultados
          this.filtro='Peliculas'
          
        },
        error: (err) => {console.log(err)},
        complete: () => {console.log('Ejecucion completa')},
      }
    )
    this.cantidad = this.auxBuscador.length
  }

  getSeries() {
    this._movieService.getSeries().subscribe(
      {
        next: (data:TvPopular) => {
          this.auxSeries=data
          this.auxResultados = this.auxSeries['results']
          this.auxBuscador = this.auxResultados
          this.filtro='Series'
          
        },
        error: (err) => {console.log(err)},
        complete: () => {console.log('Ejecucion completa')},
      }
    )
    this.cantidad = this.auxBuscador.length
  }

  buscar() {
    this.auxBuscador = []
    for (let element of this.auxResultados){
      if (element.title.toLowerCase().includes(this.buscador.toLowerCase())){
        this.auxBuscador.push(element)
      }
    }
    this.cantidad = this.auxBuscador.length
  }
}
