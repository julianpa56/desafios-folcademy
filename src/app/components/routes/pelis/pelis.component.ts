import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MoviePopular } from 'src/app/interfaces/MoviePopular';
import { AuthServiceService } from 'src/app/services/AuthService/auth-service.service';

@Component({
  selector: 'app-pelis',
  templateUrl: './pelis.component.html',
  styleUrls: ['./pelis.component.css']
})
export class PelisComponent implements OnInit {

  auxMovies: MoviePopular | undefined;
  auxResultados : any = [];
  auxBuscador : any = []
  buscador : string = ''

  // Usuario  
  usuario : any 
  usuarioJSON : string|null = null

  // Movies Favoritas

  moviesFav : any[]=[]

  cantidad : number = 0
  cantidadFav : number = 0

  constructor( private authService : AuthServiceService, private _movieService : MoviesService ) { }

  ngOnInit(): void {
    this.getMovies()
    this.getMoviesFav()
  }

  getMovies() {
    this._movieService.getMovies().subscribe(
      {
        next: (data:MoviePopular) => {
          this.auxMovies=data
          this.auxResultados = this.auxMovies['results']
          this.auxBuscador = this.auxResultados 
        },
        error: (err) => {console.log(err)},
        complete: () => {console.log('Ejecucion completa')},
      }
    )
    this.cantidad = this.auxBuscador.length
  }

  buscar() {
    this.auxBuscador = []
    for (let element of this.auxResultados){
      if (element.title.toLowerCase().includes(this.buscador.toLowerCase())){
        this.auxBuscador.push(element)
      }
    }
    this.cantidad = this.auxBuscador.length
  }


   // Usuario loggeado

   usuarioLoggeado = this.authService.getUserLogged()

   getMoviesFav(){
     this.getLocalStorage()
 
     this._movieService.getMoviesFav(this.usuario.uid).subscribe(data=> {
      this.moviesFav= []
       data.forEach((element:any) => {
         this.moviesFav.push({
           ide: element.payload.doc.id,
           ...element.payload.doc.data()
         })
       });
     })
     console.log(this.moviesFav)
     this.cantidadFav = this.moviesFav.length
   }
 
   getLocalStorage() {
     this.usuarioJSON = localStorage.getItem('Usuario')
     if (this.usuarioJSON) {
       this.usuario= JSON.parse(this.usuarioJSON)
       return true
     }
     else {
       return false
     }
   }
}
