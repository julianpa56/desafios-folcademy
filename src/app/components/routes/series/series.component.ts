import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { TvPopular } from 'src/app/interfaces/TvPopular';
import { AuthServiceService } from 'src/app/services/AuthService/auth-service.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {

  auxSeries: TvPopular | undefined;
  auxResultados : any = [];
  auxBuscador : any = []
  buscador : string = ''

  // Usuario  
  usuario : any 
  usuarioJSON : string|null = null

  // Movies Favoritas

  moviesFav : any[]=[]

  cantidad : number = 0
  cantidadFav : number = 0

  constructor( private authService : AuthServiceService, private _movieService : MoviesService ) { }

  ngOnInit(): void {
    this.getSeries()
    this.getMoviesFav()
  }

  getSeries() {
    this._movieService.getSeries().subscribe(
      {
        next: (data:TvPopular) => {
          this.auxSeries=data
          this.auxResultados = this.auxSeries['results']
          this.auxBuscador = this.auxResultados          
        },
        error: (err) => {console.log(err)},
        complete: () => {console.log('Ejecucion completa')},
      }
    )
    this.cantidad = this.auxBuscador.length
  }

  
  buscar() {
    this.auxBuscador = []
    for (let element of this.auxResultados){
      if (element.title.toLowerCase().includes(this.buscador.toLowerCase())){
        this.auxBuscador.push(element)
      }
    }
    this.cantidad = this.auxBuscador.length
  }

  // Usuario loggeado

  usuarioLoggeado = this.authService.getUserLogged()

  getMoviesFav(){
    this.getLocalStorage()
    this.moviesFav= []

    this._movieService.getMoviesFav(this.usuario.uid).subscribe(data=> {
      data.forEach((element:any) => {
        this.moviesFav.push({
          ide: element.payload.doc.id,
          addedMovie: element.type,
          ... element.payload.doc.data()
        })
      });
    })
    console.log(this.moviesFav)
    this.cantidadFav = this.moviesFav.length
  }

  getLocalStorage() {
    this.usuarioJSON = localStorage.getItem('Usuario')
    if (this.usuarioJSON) {
      this.usuario= JSON.parse(this.usuarioJSON)
      return true
    }
    else {
      return false
    }
  }

}
