import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthServiceService } from 'src/app/services/AuthService/auth-service.service';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css'],
  providers: [AuthServiceService]
})


export class IngresarComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.minLength(3),Validators.email]],
    contrasena: [, [Validators.required, Validators.minLength(3)]],
  });

  usuarioLoggeado = this.authService.getUserLogged()
  
  usuario : any 
  usuarioJSON : string|null = null

  constructor( private fb: FormBuilder , private authService: AuthServiceService) { }


  ngOnInit(): void {
    this.miFormulario.reset({
      email: '',
      contrasena: '',
    });
  }

  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    )
  }

  registrar() {
    const { email , contrasena } = this.miFormulario.value
    this.authService.register(email,contrasena).then(res => {
      console.log("se registro: ",res)
    })
    this.miFormulario.reset();
  };

  ingresar() {
      const { email , contrasena } = this.miFormulario.value
      this.authService.login(email,contrasena).then(res => {
        console.log("inicio de sesion: ",res)
        this.setLocalStorage(res?.user);
        this.guardarUsuario()
      })
      this.miFormulario.reset();
    };

    ingresarConGoogle() {
      this.authService.loginWithGoogle().then(res => {
        console.log(res)
        this.setLocalStorage(res?.user)
      })
      this.miFormulario.reset();
    };

    obtenerUsuario(){
      this.authService.getUserLogged().subscribe(res => {
        console.log(res?.email)
      })
    }

    cerrarSesion(){
      this.authService.logOut()
      this.removeLocalStorage()
    }

    public setLocalStorage(data:any) {
      localStorage.setItem('Usuario', JSON.stringify(data));
    }
  
    public getLocalStorage() {
      this.usuarioJSON = localStorage.getItem('Usuario')
      if (this.usuarioJSON) {
        this.usuario= JSON.parse(this.usuarioJSON)
        return true
      }
      else {
        return false
      }
    }
  
    removeLocalStorage() {
      localStorage.removeItem('Usuario');
    }


    guardarUsuario(){
      this.getLocalStorage()
    }
    
} 



