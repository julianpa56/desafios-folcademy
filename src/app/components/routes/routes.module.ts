import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { PelisComponent } from './pelis/pelis.component';
import { SeriesComponent } from './series/series.component';
import { IngresarComponent } from './ingresar/ingresar.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { RegistrarseComponent } from './registrarse/registrarse.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AgregarComponent } from './agregar/agregar.component';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    InicioComponent,
    PelisComponent,
    SeriesComponent,
    IngresarComponent,
    RegistrarseComponent,
    AgregarComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AppRoutingModule
  ],
  exports: [
    InicioComponent,
    PelisComponent,
    SeriesComponent,
    IngresarComponent,
    RegistrarseComponent
  ]
})
export class RoutesModule { }
