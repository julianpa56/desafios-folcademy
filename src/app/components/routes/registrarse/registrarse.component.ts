import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthServiceService } from 'src/app/services/AuthService/auth-service.service';


@Component({
  selector: 'app-registrarse',
  templateUrl: './registrarse.component.html',
  styleUrls: ['./registrarse.component.css']
})
export class RegistrarseComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.minLength(3),Validators.email]],
    contrasena: [, [Validators.required, Validators.minLength(3)]],
  });

  usuario : any
  usuarioJSON : string|null = null

  constructor( private fb: FormBuilder , private authService: AuthServiceService) { }


  ngOnInit(): void {
    this.miFormulario.reset({
      email: '',
      contrasena: '',
    });
  }

  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    )
  }

  registrar() {
    const { email , contrasena } = this.miFormulario.value
    this.authService.register(email,contrasena).then(res => {
      console.log("se registro: ",res)
      this.removeLocalStorage()
      this.authService.logOut()
    })
    this.miFormulario.reset();
  };
    
  removeLocalStorage() {
    localStorage.removeItem('Usuario');
  }
  

} 




