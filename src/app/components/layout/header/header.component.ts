import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from 'src/app/services/AuthService/auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  usuario : any
  usuarioJSON : string|null = null

  constructor(private authService: AuthServiceService) { }

  ngOnInit(): void {
  }

  cerrarSesion(){
    this.authService.logOut()
    this.removeLocalStorage()
    this.usuario= null
  }

  usuarioLoggeado = this.authService.getUserLogged()

  getLocalStorage() {
    this.usuarioJSON = localStorage.getItem('Usuario')
    if (this.usuarioJSON) {
      this.usuario= JSON.parse(this.usuarioJSON)
      return true
    }
    else {
      return false
    }
  }

  removeLocalStorage() {
    localStorage.removeItem('Usuario');
  }

}
