import { Component, Input, OnInit } from '@angular/core';
import { MovieSerie, MovieSerieBase, MovieSerieUser } from 'src/app/interfaces/MovieSerie';
import { Result, Trending } from 'src/app/interfaces/Trending';
import { AuthServiceService } from 'src/app/services/AuthService/auth-service.service';
import { MoviesService } from 'src/app/services/movies/movies.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  usuario : any 
  usuarioJSON : string|null = null
  
@Input() aux : any = []
@Input() filtro : any = 'todos'



constructor (private authService : AuthServiceService, private moviesService: MoviesService){}

  ngOnInit(): void {
  }

  agregarMovie(movie:any){

    this.getLocalStorage()


    if(this.usuario){
      this.moviesService.addMovie(this.usuario.uid,movie)
      console.log('objeto agregado')
    }
    else {
      console.log('no esta loggeado')
    }
  }


  eliminarMovie(idMovie: any){
    this.getLocalStorage()
    if(this.usuario){
      this.moviesService.deleteItem(this.usuario.uid,idMovie.toString())
      // console.log(idMovie)
      console.log('objeto eliminado')
    }
    else {
      console.log('no esta loggeado')
    }  
  }

  getLocalStorage() {
    this.usuarioJSON = localStorage.getItem('Usuario')
    if (this.usuarioJSON) {
      this.usuario= JSON.parse(this.usuarioJSON)
      return true
    }
    else {
      return false
    }
  }

  usuarioLoggeado = this.authService.getUserLogged()

}
