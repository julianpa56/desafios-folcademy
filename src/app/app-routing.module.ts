import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/routes/dashboard/dashboard.component';
import { IngresarComponent } from './components/routes/ingresar/ingresar.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { PelisComponent } from './components/routes/pelis/pelis.component';
import { RegistrarseComponent } from './components/routes/registrarse/registrarse.component';
import { SeriesComponent } from './components/routes/series/series.component';


const routes: Routes = [
  {path:'', component: InicioComponent},
  {path:'inicio',component: InicioComponent},
  {path:'peliculas',component: PelisComponent},
  {path:'series',component: SeriesComponent},
  {path:'ingresar',component: IngresarComponent},
  {path:'ingresar/registrar',component: RegistrarseComponent},
  {path:'ingresar/registrar/ingresar',redirectTo:'ingresar'},
  {path:'dashboard', component: DashboardComponent},
  {path:'dashboard/peliculas',redirectTo:'peliculas'},
  {path:'dashboard/series',redirectTo:'series'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),BrowserModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
