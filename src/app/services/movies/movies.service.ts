import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Trending } from 'src/app/interfaces/Trending';
import { Observable } from 'rxjs';
import { MoviePopular } from 'src/app/interfaces/MoviePopular';
import { TvPopular } from 'src/app/interfaces/TvPopular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MovieSerieBase } from 'src/app/interfaces/MovieSerie';



@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  

  page=1

  appId: string = '22725ffe9e29b678b0ff1ef3842e1f08'

  baseUrl: string = 'https://api.themoviedb.org/3'

  constructor( private _http: HttpClient, private firestore: AngularFirestore  ) { 
  }

  getTrending() : Observable< Trending > {

      return this._http.get<Trending>(this.baseUrl + '/trending/all/week?' + 'api_key=' + this.appId + '&' + 'language=es' + '&' + 'page=' + this.page);
  }

  getMovies() : Observable < MoviePopular > {

      return this._http.get<MoviePopular>(this.baseUrl + '/movie/popular?' + 'api_key=' + this.appId + '&' + 'language=es' + '&' + 'page=' + this.page);
  
  }

  getSeries() : Observable < TvPopular > {

      return this._http.get<TvPopular>(this.baseUrl + '/tv/popular?' + 'api_key=' + this.appId + '&' + 'language=es' + '&' + 'page=' + this.page);
  }

  addMovie(userId:string, item:MovieSerieBase): Promise<any> {
    return this.firestore.collection('Usuarios').doc(`${userId}`).collection('list').add(item)
      .then(res => {
        return res.id
      })
  }

  getMoviesFav(userId:string): Observable<any> {
    return this.firestore.collection(`Usuarios/${userId}/list`).snapshotChanges()
  }


  
  deleteItem(userId:string, id:string): Promise<any> {
    console.log(id)
    return this.firestore.collection(`Usuarios/${userId}/list`).doc(id).delete().then(()=> console.log('borrado')).catch((error)=> console.log(error))
  }

}
