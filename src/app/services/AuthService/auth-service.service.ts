import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth'
import { AngularFirestore } from '@angular/fire/compat/firestore';
import firebase from 'firebase/compat/app';
import { userInfo } from 'os';
import { MovieSerieUser } from 'src/app/interfaces/MovieSerie';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  
  constructor(private afauth : AngularFireAuth , private firestore : AngularFirestore) {  }


  async register(email: string , contrasena: string){
    try {
      return await this.afauth.createUserWithEmailAndPassword(email,contrasena)
    }
    catch (err) {
      console.log("error en login: ", err)
      return null;
    }
  }

  async login(email: string , contrasena: string){
    try {
      return await this.afauth.signInWithEmailAndPassword(email,contrasena)
    }
    catch (err) {
      console.log("error en login: ", err)
      return null;
    }
  }

  async loginWithGoogle() {
    try {
      return await this.afauth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    }
    catch (err){
      console.log("error en login con google: ", err)
      return null
    }
  }

  getUserLogged(){
    return this.afauth.authState
  }

  logOut(){
    this.afauth.signOut().then(function() {

      console.log("Sesion Cerrada")
    })
    .catch(function(error) {
      console.log("error al cerrar sesion")
    });
  }

  // setUser(idUsuario:any) {

  //   console.log(idUsuario)
  //   if(idUsuario != 'null'){
  //     const usuario = this.firestore.collection('Usuarios').add({idUsuario}).then(res => {
  //       console.log('DB de usuario creada')
  //       console.log(usuario)
  //     }).catch(error => {
  //       console.log(error)
  //     })
  //   }

  // }

}
