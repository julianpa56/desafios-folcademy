export interface Trending {
    id:            any;
    page:          number;
    results:       Result[];
    total_pages:   number;
    total_results: number;
}

export interface Result {
    name?:             string;
    genre_ids:         number[];
    origin_country?:   string[];
    original_language: string;
    first_air_date?:   Date;
    poster_path:       string;
    vote_average:      number;
    id:                number;
    vote_count:        number;
    overview:          string;
    original_name?:    string;
    backdrop_path:     string;
    popularity:        number;
    media_type:        string;
    release_date?:     Date;
    title?:            string;
    adult?:            boolean;
    original_title?:   string;
    video?:            boolean;
}