// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCetTelNo7G0wJqrHHMgC7pC_56TeiLBTw",
    authDomain: "moviesandseriesapp.firebaseapp.com",
    projectId: "moviesandseriesapp",
    storageBucket: "moviesandseriesapp.appspot.com",
    messagingSenderId: "664629286912",
    appId: "1:664629286912:web:b7b91469ff081f1c97e0e5",
    measurementId: "G-0QSHGED514"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
